package com.kc.api.service.lmp;

import com.kc.api.mapper.ApiSignMapper;
import com.kc.api.model.SignEntity;
import com.kc.api.service.ApiSignService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/5/3 12:53
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class ApiSignServicelmp implements ApiSignService {
   @Autowired
    private ApiSignMapper apiSignMapper;
    @Override
    public Integer signSave(SignEntity signEntity) {
        return apiSignMapper.signSave(signEntity);
    }

    @Override
    public SignEntity querySignList(Integer stu_id, Integer course_id, String queryDate, String queryDateEnd) {
        return apiSignMapper.querySignList(stu_id,course_id,queryDate,queryDateEnd);
    }

    @Override
    public List<SignEntity> querylistByUserId(Long userId) {
        return apiSignMapper.querylistByUserId(userId);
    }

    @Override
    public List<SignEntity> querySignlistByUserId(Long userId) {
        return apiSignMapper.querySignlistByUserId(userId);
    }
}
