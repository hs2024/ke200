package com.kc.api.controller;

import com.kc.api.annotation.LoginUser;
import com.kc.api.common.ApiRRException;
import com.kc.api.common.R;
import com.kc.api.model.ClassTimeEntity;
import com.kc.api.model.SignEntity;
import com.kc.api.model.User;
import com.kc.api.service.ApiSignService;
import com.kc.api.service.ApiUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * author shish
 * Create Time 2019/5/2 16:20
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sign")
public class ApiSignController {
    @Autowired
    private ApiUserService apiUserService;
    @Autowired
    private ApiSignService apiSignService;

    /**
     * 签到签到数据
     */
    @PostMapping("/get")
    public R getStudentSignList(@RequestBody @LoginUser User user) {
        List<ClassTimeEntity> list = apiUserService.getClassTimeEntities(user);
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        return R.ok(result);
    }


    /**
     * 签到接口
     */
    @RequestMapping("/start")
    public R startSign(@RequestBody @LoginUser SignModel signModel) throws ParseException {
        User user = null;
        SignEntity signEntity = null;
        SimpleDateFormat sdf = null;
        Map<String, Object> result = null;
        Date queryDate = null;
        Date queryDateEnd = null;
        try {
            user = signModel.getUser();
            signEntity = signModel.getSignEntity();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            result = new HashMap<>();
            queryDate = new Date(System.currentTimeMillis());
            queryDate.setHours(0);
            queryDate.setMinutes(0);
            queryDate.setSeconds(0);
            queryDateEnd = new Date(System.currentTimeMillis());
            queryDateEnd.setHours(23);
            queryDateEnd.setMinutes(59);
            queryDateEnd.setSeconds(59);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiRRException("参数错误请检查参数");
        }
        Integer userId = Math.toIntExact(user.getUserId());
        SignEntity sign = apiSignService.querySignList(userId, signEntity.getCourse_id(), sdf.format(queryDate), sdf.format(queryDateEnd));
        if (sign != null) {
            result.put("code", 2000);
            return R.ok(result);
        }
        Date date = new Date(System.currentTimeMillis());
        signEntity.setSign_time(date);
        Date db_date = sdf.parse(signEntity.getComp_time());
        if (db_date.before(date)) {
            signEntity.setSign_status(0);
            result.put("code", 3000);
        }
        signEntity.setSign_status(1);
        signEntity.setGrade(user.getGrade());
        Integer code = apiSignService.signSave(signEntity);
        try {
            result.put("code", code);
        } catch (Exception e) {
            code = 1000;
            result.put("code", code);
            e.printStackTrace();
        }
        return R.ok(result);
    }

    @RequestMapping("/total")
    public R total(@LoginUser User user) {
        List<SignEntity> signlist = apiSignService.querylistByUserId(user.getUserId());
        Map<String, Object> result = new HashMap<>();
        result.put("list", signlist);
        return R.ok(result);
    }

    @RequestMapping("/untotal")
    public R untotal(@LoginUser User user) {
        List<SignEntity> signlist = apiSignService.querySignlistByUserId(user.getUserId());
        Map<String, Object> result = new HashMap<>();
        result.put("list", signlist);
        return R.ok(result);
    }

    public static Long dateFormate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        Date d = sdf.parse(date);
        long ts = d.getTime();
        return ts;
    }
}
