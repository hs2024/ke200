package com.kc.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * author shish
 * Create Time 2019/4/29 16:22
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public class SignEntity implements Serializable {
    private Integer id;
    private Integer kc_class_time_id;
    private Integer stu_id;
    private Date sign_time;
    private Integer sign_status;
    private String name;
    //课程id
    private Integer course_id;
    //经度
    private String longtitude;
    //维度
    private String lantitude;
    //年级信息
    private String grade;
    private String course_name;
    private String comp_time;
    private String end_time;
    //签到地址描述
    private String desc;
    //正常签到次数
    private Integer num;
    private String stu_name;
    //小程序端显示的状态0、1
    private Integer status;
    //迟到的次数
    private Integer normalNum;

    public Integer getNormalNum() {
        return normalNum;
    }

    public void setNormalNum(Integer normalNum) {
        this.normalNum = normalNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getComp_time() {
        return comp_time;
    }

    public void setComp_time(String comp_time) {
        this.comp_time = comp_time;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLantitude() {
        return lantitude;
    }

    public void setLantitude(String lantitude) {
        this.lantitude = lantitude;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKc_class_time_id() {
        return kc_class_time_id;
    }

    public void setKc_class_time_id(Integer kc_class_time_id) {
        this.kc_class_time_id = kc_class_time_id;
    }

    public Integer getStu_id() {
        return stu_id;
    }

    public void setStu_id(Integer stu_id) {
        this.stu_id = stu_id;
    }

    public Date getSign_time() {
        return sign_time;
    }

    public void setSign_time(Date sign_time) {
        this.sign_time = sign_time;
    }

    public Integer getSign_status() {
        return sign_status;
    }

    public void setSign_status(Integer sign_status) {
        this.sign_status = sign_status;
    }
}
