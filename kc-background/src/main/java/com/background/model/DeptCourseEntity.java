package com.background.model;

import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/3/9 16:21
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public class DeptCourseEntity implements Serializable {
    private  Integer id;
    private  Integer uid;
    private  Integer dept_id;
    private  String  dept_name;//学院号，比如理学院11
    private  String  dept_class;//学院班级，比如理学院1511211班级
    private  Integer course_id;//课程id
    private  String  course_code;//课程编号
    private String   course_name;
    private  String  course_teacher;



    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }


    public String getDept_class() {
        return dept_class;
    }

    public void setDept_class(String dept_class) {
        this.dept_class = dept_class;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }


    public String getCourse_code() {
        return course_code;
    }

    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_teacher() {
        return course_teacher;
    }

    public void setCourse_teacher(String course_teacher) {
        this.course_teacher = course_teacher;
    }

    public Integer getDept_id() {
        return dept_id;
    }

    public void setDept_id(Integer dept_id) {
        this.dept_id = dept_id;
    }
}
