package com.background.mapper;

import com.background.model.DeptEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * author shish
 * Create Time 2019/3/8 15:27
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface DeptMapper extends BaseMapper<DeptEntity> {
    int insertDept(DeptEntity deptEntity);
}
