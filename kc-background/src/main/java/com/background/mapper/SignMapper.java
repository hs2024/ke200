package com.background.mapper;

import com.background.model.SignEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 16:33
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface SignMapper  extends BaseMapper<SignEntity> {

    //正常签到的人数
    Integer normal(@Param("class_grade") String class_grade, @Param("start") String start, @Param("end") String end);
    //不正常签到的人数
    List<SignEntity> unNormal(@Param("class_grade") String class_grade, @Param("start") String start, @Param("end") String end);


    //正常签到的学生
    List<SignEntity> NormalList(@Param("class_grade") String class_grade,@Param("start") String start,@Param("end") String end);
    //没有正常签到的学生
    List<SignEntity>  unNormalList(@Param("class_grade") String class_grade,@Param("start") String start,@Param("end") String end);

    List<SignEntity> match(@Param("class_grade") String class_grade, @Param("course_id") Integer course_id);
}
