package com.background.mapper;

import com.background.model.DeptClassTeacherCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 15:27
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ClassTimeMapper extends BaseMapper<DeptClassTeacherCourse> {
     List<DeptClassTeacherCourse> queryListByUserId(Integer uid);
}
