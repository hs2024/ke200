package com.background.shiro;

import com.background.mapper.PermissionMapper;
import com.background.mapper.RoleMapper;
import com.background.mapper.UserMapper;
import com.background.model.Permission;
import com.background.model.Role;
import com.background.model.User;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.Permissions;
import java.util.Set;


///**
// * author shish
// * Create Time 2019/1/12 14:04
// * author email shisheng@live.com
// * website www.bangnila.com
// **/
//public class ShiroRealm extends AuthorizingRealm {
//    @Autowired
//    private UserMapper userMapper;
//
//    @Autowired
//    private RoleMapper roleMapper;
//
//    @Autowired
//    private PermissionMapper permissionMapper;
//    /**
//     * 验证用户身份
//     * @param authenticationToken
//     * @return
//     * @throws AuthenticationException
//     */
//    @Override
//    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
//        System.out.println("---------------------------------进入认证页面-------------------------------------------");
//        //获取用户名密码 第一种方式
//        //String username = (String) authenticationToken.getPrincipal();
//        //String password = new String((char[]) authenticationToken.getCredentials());
//
//        //获取用户名 密码 第二种方式
//        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
//        String username = usernamePasswordToken.getUsername();
//        String password = new String(usernamePasswordToken.getPassword());
//        System.out.println(password);
//        //从数据库查询用户信息
//        QueryWrapper<User> queryWrapper=new QueryWrapper<>();
//        queryWrapper.eq("username",username);
//        User user = this.userMapper.selectOne(queryWrapper);
//
//        //可以在这里直接对用户名校验,或者调用 CredentialsMatcher 校验
//        if (user == null) {
//            throw new UnknownAccountException("用户名或密码错误！");
//        }
////        if (!password.equals(user.getPassword())) {
////            throw new IncorrectCredentialsException("用户名或密码错误！");
////        }
//        if ("1".equals(user.getState())) {
//            throw new LockedAccountException("账号已被锁定,请联系管理员！");
//        }
//
//        //调用 CredentialsMatcher 校验 还需要创建一个类 继承CredentialsMatcher  如果在上面校验了,这个就不需要了
//        //配置自定义权限登录器 参考博客：
//
//        return new SimpleAuthenticationInfo(username, password,getName());
//    }
//
//    /**
//     * 授权用户权限
//     * 授权的方法是在碰到<shiro:hasPermission name=''></shiro:hasPermission>标签的时候调用的
//     * 它会去检测shiro框架中的权限(这里的permissions)是否包含有该标签的name值,如果有,里面的内容显示
//     * 如果没有,里面的内容不予显示(这就完成了对于权限的认证.)
//     *
//     * shiro的权限授权是通过继承AuthorizingRealm抽象类，重载doGetAuthorizationInfo();
//     * 当访问到页面的时候，链接配置了相应的权限或者shiro标签才会执行此方法否则不会执行
//     * 所以如果只是简单的身份认证没有权限的控制的话，那么这个方法可以不进行实现，直接返回null即可。
//     *
//     * 在这个方法中主要是使用类：SimpleAuthorizationInfo 进行角色的添加和权限的添加。
//     * authorizationInfo.addRole(role.getRole()); authorizationInfo.addStringPermission(p.getPermission());
//     *
//     * 当然也可以添加set集合：roles是从数据库查询的当前用户的角色，stringPermissions是从数据库查询的当前用户对应的权限
//     * authorizationInfo.setRoles(roles); authorizationInfo.setStringPermissions(stringPermissions);
//     *
//     * 就是说如果在shiro配置文件中添加了filterChainDefinitionMap.put("/add", "perms[权限添加]");
//     * 就说明访问/add这个链接必须要有“权限添加”这个权限才可以访问
//     *
//     * 如果在shiro配置文件中添加了filterChainDefinitionMap.put("/add", "roles[100002]，perms[权限添加]");
//     * 就说明访问/add这个链接必须要有 "权限添加" 这个权限和具有 "100002" 这个角色才可以访问
//     * @param principalCollection
//     * @return
//     */
//    @Override
//    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//        System.out.println("---------------------------------进入授权页面-------------------------------------------");
//        //获取用户
//        User user = (User) SecurityUtils.getSubject().getPrincipal();
//
//        //获取用户角色
//        Set<Role> roles =this.roleMapper.findRolesByUserId(user.getUid());
//        //添加角色
//        SimpleAuthorizationInfo authorizationInfo =  new SimpleAuthorizationInfo();
//        for (Role role : roles) {
//            authorizationInfo.addRole(role.getRole());
//        }
//
//        //获取用户权限
//        Set<Permission> permissions = this.permissionMapper.findPermissionsByRoleId(roles);
//        //添加权限
//        for (Permission permission:permissions) {
//            authorizationInfo.addStringPermission(permission.getPermission());
//        }
//
//        return authorizationInfo;
//    }
//    /**
//     * 重写方法,清除当前用户的的 授权缓存
//     * @param principals
//     */
//    @Override
//    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
//        super.clearCachedAuthorizationInfo(principals);
//    }
//
//    /**
//     * 重写方法，清除当前用户的 认证缓存
//     * @param principals
//     */
//    @Override
//    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
//        super.clearCachedAuthenticationInfo(principals);
//    }
//
//    @Override
//    public void clearCache(PrincipalCollection principals) {
//        super.clearCache(principals);
//    }
//
//    /**
//     * 自定义方法：清除所有 授权缓存
//     */
//    public void clearAllCachedAuthorizationInfo() {
//        getAuthorizationCache().clear();
//    }
//
//    /**
//     * 自定义方法：清除所有 认证缓存
//     */
//    public void clearAllCachedAuthenticationInfo() {
//        getAuthenticationCache().clear();
//    }
//
//    /**
//     * 自定义方法：清除所有的  认证缓存  和 授权缓存
//     */
//    public void clearAllCache() {
//        clearAllCachedAuthenticationInfo();
//        clearAllCachedAuthorizationInfo();
//    }
//
//}
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;

import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.management.relation.RoleNotFoundException;

@Slf4j
public class CustomRealm extends AuthorizingRealm {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private PermissionMapper permissionMapper;

    /**
     * @MethodName doGetAuthorizationInfo
     * @Description 权限配置类
     * @Param [principalCollection]
     * @Return AuthorizationInfo
     * @Author WangShiLin
     */
    @SneakyThrows
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取登录用户名
        log.info("授权开始");
        User user1 = (User) principalCollection.getPrimaryPrincipal();
        //查询用户名称
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", user1.getUsername());
        User user = userMapper.selectOne(queryWrapper);
        //添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        Set<Role> roles = this.roleMapper.findRolesByUserId(user.getUid());
        if (roles.size() == 0) {
            throw new RoleNotFoundException("角色不存在");
        }
        //添加角色
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        for (Role role : roles) {
            authorizationInfo.addRole(role.getRole());
        }

        //获取用户权限
        Set<Permission> permissions = this.permissionMapper.findPermissionsByRoleId(roles);
        //添加权限
        for (Permission permission : permissions) {
            authorizationInfo.addStringPermission(permission.getPermission());
        }
        return simpleAuthorizationInfo;
    }

    /**
     * @MethodName doGetAuthenticationInfo
     * @Description 认证配置类
     * @Param [authenticationToken]
     * @Return AuthenticationInfo
     * @Author WangShiLin
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("认证开始");
        if (StringUtils.isEmpty(authenticationToken.getPrincipal())) {
            return null;
        }
        //获取用户信息
        String username = authenticationToken.getPrincipal().toString();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            //这里返回后会报出对应异常
            return null;
        } else {
            //这里验证authenticationToken和simpleAuthenticationInfo的信息
            UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
            char[] password = usernamePasswordToken.getPassword();
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user, password, getName());

            return simpleAuthenticationInfo;
        }
    }
}
