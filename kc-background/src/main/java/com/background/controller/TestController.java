package com.background.controller;

import com.background.model.MenuEntity;
import com.background.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author shish
 * Create Time 2019/3/7 16:15
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("test")
public class TestController {
     @Autowired
    private TestService testService;
    @RequestMapping("/index")
    public int test(MenuEntity menuEntity){

        return testService.test(menuEntity);
    }
}
