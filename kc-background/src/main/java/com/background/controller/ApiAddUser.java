package com.background.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author shish
 * Create Time 2019/1/13 11:08
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("/user")
public class ApiAddUser {
     //权限管理


    @RequestMapping("/add")
    @RequiresPermissions("userInfo:del")
    public String  add_user() {
        return "给admin用户添加 userInfo:del 权限成功";
    }

    @RequestMapping("/delete")
    @RequiresPermissions("userInfo:del")
    public String  delete_user() {
        return "给admin用户添加 userInfo:del 权限成功";
    }


}
