package com.background.service.lmp;

import com.background.mapper.StudentMapper;
import com.background.model.StudentEntity;
import com.background.service.StudentService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 11:09
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class StudentServicelmp implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public List<StudentEntity> selectList(String class_grade, Integer uid) {
        return studentMapper.queryList(class_grade,uid);
    }

    @Override
    public List<StudentEntity> selectListPage(Page<StudentEntity> objectPage,QueryWrapper<StudentEntity> queryWrapper) {
        IPage<StudentEntity> studentEntityIPage = studentMapper.selectPage(objectPage, queryWrapper);
        return studentEntityIPage.getRecords() ;
    }

    @Override
    public Integer save(StudentEntity studentEntity) {
        return studentMapper.insert(studentEntity);
    }

    @Override
    public StudentEntity selectOne(Long userId) {
        return studentMapper.selectById(userId);
    }
}
